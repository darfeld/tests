#include <iostream>
#include <string.h>

using namespace std;

int main(){
    char * string1 = "Ceci est une string sans piège.";
    char * itsatrap = "Cela est une \0string piégée.";


    char tmp[64];

    cout << "tmp est à l'adresse " << &tmp << endl;

    memcpy(tmp, string1, strlen(string1)+1);

    cout << "la première string fait : " << strlen(string1) << " caractères." << endl;
    cout << "la première string est : " << tmp << endl;
    cout << "tmp est à l'adresse " << &tmp << endl;

    memcpy(tmp, itsatrap, 31);
    cout << "la seconde string fait : " << strlen(itsatrap) + strlen(itsatrap + 14) << " caractères." << endl;
    cout << "la seconde string est : " << tmp << (tmp+14) << endl;
    cout << "tmp est à l'adresse " << &tmp << endl;
}
