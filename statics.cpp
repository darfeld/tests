#include <iostream>
#include "uniqueID.h"

int main()
{
    for(int i=0; i < 10; i++)
    {
        std::cout << "Id #" << i << " = " << UniqueId::getId() << std::endl;
    }
    return 0;
}
