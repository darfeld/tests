#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <cstring>

using namespace std;

void send_multicast(char* interface);

int main(int argc, char ** argv)
{
	send_multicast(argv[1]);

	return 0;
}

void send_multicast(char* interface)
{
	int hSocket = socket(AF_INET, SOCK_DGRAM, 0);

	struct sockaddr_in addr_dest;
	addr_dest.sin_family = AF_INET;
	addr_dest.sin_addr.s_addr = inet_addr("225.0.0.1");
	addr_dest.sin_port = htons(50000);

	unsigned char ttl=1;
	if (interface != 0)
	{
		int rc = setsockopt(hSocket, SOL_SOCKET, SO_BINDTODEVICE, interface, sizeof(interface)+1);
		if (rc != 0) 
		{
			cerr << "couldn't bind multicast socket to " << interface << ": " << strerror(errno) << endl;
			exit(1);
		}

		char buffer[10];
		socklen_t opt_len;
		rc = getsockopt(hSocket, SOL_SOCKET, SO_BINDTODEVICE, buffer, &opt_len);
		if (rc != 0)
		{
			cerr << "couldn't read which interface was binded with multicast socket." << endl;
			exit(2);
		}
		cout << "Socket binded to " << buffer << endl;
	}
	setsockopt(hSocket, IPPROTO_IP, IP_MULTICAST_TTL, (char *)&ttl, sizeof(ttl));

	while (1)
	{
		sendto(hSocket, "hello", 6, 0, (struct sockaddr *)&addr_dest, sizeof(addr_dest));
		sleep(1);
	}
}
