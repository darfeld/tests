#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <cstring>

using namespace std;

const int MAXLEN = 2048;

void receive_multicast(char* interface);

int main(int argc, char ** argv)
{
	receive_multicast(argv[1]);

	return 0;
}

void receive_multicast(char* interface)
{
	int hSocket = socket(AF_INET, SOCK_DGRAM, 0);
	if( hSocket == 0)
	{
		cerr << "couldn't allocate socket: " << strerror(errno) << endl;
		exit(10);
	}

	struct sockaddr_in group;
	struct sockaddr_in from;
	group.sin_family = AF_INET;
	group.sin_addr.s_addr = inet_addr("225.0.0.1");
	group.sin_port = htons(50000);
	u_int yes=1;
	u_char no=0;
	struct ip_mreq mreq;
	char message[MAXLEN];

	int rc, n;
	socklen_t len;

	unsigned char ttl=1;
	if (interface != 0)
	{
		rc = setsockopt(hSocket, SOL_SOCKET, SO_BINDTODEVICE, interface, sizeof(char)*(strlen(interface)+1));
		if (rc != 0) 
		{
			cerr << "couldn't bind multicast socket to " << interface << ": " << strerror(errno) << endl;
			exit(1);
		}

		char buffer[10];
		socklen_t opt_len;
		rc = getsockopt(hSocket, SOL_SOCKET, SO_BINDTODEVICE, buffer, &opt_len);
		if (rc != 0)
		{
			cerr << "couldn't read which interface was binded with multicast socket :" << strerror(errno) << endl;
			exit(2);
		}
		cout << "Socket binded to " << buffer << endl;
		
	}
	rc = setsockopt(hSocket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
	if (rc != 0)
	{
		cerr << "couln't make socket reusable: " << strerror(errno) << endl;
		exit(3);
	}

	rc = bind(hSocket, (struct sockaddr*)&group, sizeof(group));
	if (rc != 0)
	{
		cerr << "couln't bind socket to multicast adress: " << strerror(errno) << endl;
		exit(4);
	}

	mreq.imr_multiaddr = group.sin_addr;
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);

	rc = setsockopt(hSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
	if (rc != 0)
	{
		cerr << "couldn't add membership to multicast: " << strerror(errno) << endl;
		exit(5);
	}

	while (1)
	{
		len = sizeof(from);
		n = recvfrom(hSocket, message, MAXLEN, 0, (struct sockaddr*)&from, &len);

		if (n<0)
		{
			cerr << "error de lecture: " << strerror(errno) << endl;
			exit(6);
		}

		message[n] = '\0';
		cout << "message reveived from " << inet_ntoa(from.sin_addr) << ": " << message << endl;
	}
}
