CC = g++
FLAGS = -std=c++98
BIN_DIR = bin

.PHONY: all clean

${BIN_DIR}/maps:
	${CC} maps.cpp ${FLAGS} -o ${BIN_DIR}/maps

${BIN_DIR}/statics:
	${CC} statics.cpp ${FLAGS} -o ${BIN_DIR}/statics

${BIN_DIR}/file_cp:
	${CC} file_cp.cpp ${FLAGS} -o ${BIN_DIR}/file_cp

${BIN_DIR}/threads:
	${CC} threads.cpp ${FLAGS} -pthread -o ${BIN_DIR}/threads

all: ${BIN_DIR}/statics ${BIN_DIR}/file_cp ${BIN_DIR}/maps

clean:
	rm -f bin/*

