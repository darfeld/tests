#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <netinet/in.h>

#include <string.h>

int main(int argc, char *argv[]) {
   int sockfd, portno, n;
   struct sockaddr_in serv_addr, client_addr;
   struct hostent *server;
   
   char buffer[256];
   
   if (argc < 3) {
      fprintf(stderr,"usage %s hostname port\n", argv[0]);
      exit(0);
   }
	
   portno = atoi(argv[2]);
   
   /* Create a socket point */
   sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }
   
   server = gethostbyname(argv[1]);
   
   if (server == NULL) {
      fprintf(stderr,"ERROR, no such host\n");
      exit(0);
   }

   memset(&client_addr, 0, sizeof(client_addr)); 
   client_addr.sin_family = AF_INET;
   bcopy((char *)server->h_addr, (char *)&client_addr.sin_addr.s_addr, server->h_length);

   if(bind(sockfd, (struct sockaddr *)&client_addr, sizeof(client_addr)) < 0) {
	   perror("ERROR on binding");
	   exit(1);
   }
   
   bzero((char *) &serv_addr, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
   serv_addr.sin_port = htons(portno);
   
   /* Now connect to the server */
   if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR connecting");
      exit(1);
   }
   
   /* Now ask for a message from the user, this message
      * will be read by server
   */
	
   printf("Please enter the message: ");
   bzero(buffer,256);
   fgets(buffer,255,stdin);
   
   char hexa_buf[255];
   for(int i=0; buffer[i] != '\0' && buffer[i+1] != '\0'; i+=2)
   {
	char octet[3] = {buffer[i], buffer[i+1], '\0'};
	hexa_buf[i/2] = (unsigned char) strtol(&(octet[0]), NULL, 16); 
	hexa_buf[i/2+1] = '\0';
   }

   /* Send message to the server */
   n = write(sockfd, hexa_buf, strlen(buffer)/2);
   
   if (n < 0) {
      perror("ERROR writing to socket");
      exit(1);
   }
   
   /* Now read server response */
   bzero(buffer,256);
   n = read(sockfd, buffer, 255);
   
   if (n < 0) {
      perror("ERROR reading from socket");
      exit(1);
   }
	
   printf("%s\n",buffer);
   return 0;
}
