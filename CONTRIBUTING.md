I mostly contribute to this. However if you want to contribute, either by 
writing a new simple test exemple or improve an existing one, please keep it 
simple. The things tested should be obvious, regular code best practice does not
apply. Just make sure it compile, without warning if possible. 

To submit change, please use merge request.