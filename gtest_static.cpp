#include "gtest/gtest.h"
#include "uniqueID.h"

class FooTest : public ::testing::Test {
	protected:
	FooTest(){
	}
	virtual ~FooTest(){
	}
};

TEST_F(FooTest, test_de_base){
	int id = UniqueId::getId();
	int id2 = UniqueId::getId();
	ASSERT_NE(id,id2);
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
