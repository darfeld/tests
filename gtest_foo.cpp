extern "C"
{
#include "foo_functions.h"
}

#include "gtest/gtest.h"


class FooTest : public ::testing::Test {
	protected:
	FooTest(){
	}
	virtual ~FooTest(){
	}
};


TEST_F(FooTest, Foo_true)
{
	int T = func_true();
	ASSERT_EQ(T,1);
}

TEST_F(FooTest, Foo_false)
{
	int F = func_false();
	ASSERT_EQ(F,1);
}

TEST(BAR, Foo_false){
	ASSERT_FALSE(func_false());
}

int main(int argc, char **argv){
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
