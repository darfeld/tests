#include <stdint.h>
#include <stdio.h>

/*
 * uint16_t size is 2 so the compilator pad after x and d.
 */
typedef struct {
	char a;
	char b;
	char x;
	uint16_t c;
	char d;
} norm;

/*
 * same as above except a and b fit in 1 char, so no padding needed after x.
 */
typedef struct {
	char a :5; // only 5 bits are used
	char b :3; // only 3 bits are used
	char x;
	uint16_t c;
	char d;
} six_char;


/**
 * no padding happening here because of the attribute packed. 
 */
typedef struct __attribute__((packed)) {
	char a;
	char b;
	char x;
	uint16_t c;
	char d;
} packed_struct;

/*
 * no padding and a and b fit in a single char.
 */
typedef struct __attribute__((packed)) {
	char a :5;
	char b :3;
	char x;
	uint16_t c;
	char d;
} packed_six;


main(){
	printf("size of normal struct : %d\n", sizeof(norm));
	printf("size of struct with ':5' : %d\n", sizeof(six_char));
	printf("size of packed struct : %d\n", sizeof(packed_struct));
	printf("size of packed struct with ':5' : %d\n", sizeof(packed_six));

}	


