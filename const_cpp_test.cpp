#include <iostream>


class A{
	public:
		int a = 0;
		int b = 1;
		const int C = 2;
		const char* ctxt = "holla";

//		Won't compile as a is read only in this function
//		void set_a(int new_a) const{
//			a= new_a;
//		}
//
		void set_b(int new_b){
			b=new_b;
		}
		const int get_const_a(){
			return a;
		}
		int get_a(){
			return a;
		}

//		const protect the whole string, so this won't compile either
//		void changeCtxtChar(int index, char c){
//			ctxt[index]=c;
//		}
};

int main(int argc, char ** argv)
{
	std::cout << "hello world" << std::endl;
}

