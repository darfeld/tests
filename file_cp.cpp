#include <iostream>
#include <fstream>
#include <string>

int main(int argc, char ** argv)
{
    std::string inFile = "rsc/loremipsum.tar.gz";
    std::string outFile = "rsc/output.tar.gz";

    if(argc > 1)
    {
        inFile = argv[1];
    }
    if(argc > 2)
    {
        outFile = argv[2];
    }

    std::fstream fi, fo;

    fi.open(inFile.c_str(), std::fstream::in | std::fstream::ate | std::fstream::binary );
    fo.open(outFile.c_str(), std::fstream::out);

    std::streampos size = fi.tellg();
    char * memblock = new char[size];

    fi.seekg(0,std::fstream::beg);

    std::cout << "size of the file is " << size << std::endl;

    fi.read(memblock, size);
    fi.close();

    fo.write(memblock, size);
    fo.close();

    delete memblock;
    return 0;
}
