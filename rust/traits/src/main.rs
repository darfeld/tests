use lib::*;

fn main() {
    let tweet = Tweet{ username: String::from("foobar_lord"),
                    content: String::from("Of course! Don't you know anything about science?"),
                    reply: false,
                    retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());
}
