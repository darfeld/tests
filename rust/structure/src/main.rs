#[derive(Debug)]
struct Rectangle{
    width:u32,
    length:u32,
}

impl Rectangle{
    fn area(&self) -> u32 {
        self.length * self.width
    }

    fn square(size:u32) -> Rectangle{
        Rectangle { width: size, length : size}
    }
}

fn main() {
    let rec1 = Rectangle { width:50, length:30};

    println!{"rec1 is {:?}", rec1};

    let square = Rectangle::square(20);

    println!{"square is {:#?}", square};
    println!("square area is {}", square.area());
}
