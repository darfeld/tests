fn main() {
    let number = 3;

    if number < 5 {
        println!("condition was true");
    } else { 
        println!("confition was false");
    }

    let condition = true;
    let number = if condition {
        5
    }else{
        6
    };

    println!("New value of number is {}",number);

    let mut i: i32 = 0;
    loop{
        i = i+1;
        println!("this is loop");
        if i > 3 { break; }
    }
    
    while i < 5 {
        println!("this is while");
        i = i+1;
    }

    for number in (1..4).rev() {
        println!("for {}!", number);
    }
}
