extern crate adder;
mod common;
use adder::adders;

#[test]
fn it_adds_two() {
    common::setup();
    assert_eq!(4, adders::add_two(2));
}
