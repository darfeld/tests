#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn larger_can_hold_smaller() {
        let larger = Rectangle { length: 8, width:7 };
        let smaller = Rectangle { length: 6, width: 4};

        assert!( larger.can_hold(&smaller));
    }

    #[test]
    fn smaller_cant_hold_larger() {
        let larger = Rectangle { length: 8, width:7 };
        let smaller = Rectangle { length: 6, width: 4};

        assert!( !smaller.can_hold(&larger));
    }

    #[test]
    fn it_adds_two() {
        assert_eq!(4, adders::add_two(2));
    }

    #[test]
    #[ignore]
    fn greeting_contains_name() {
        let result = greeting("Carol");
        assert!( result.contains("Nicole"), 
                 "Greeting did not contain name, value was '{}'", result);
    }

    #[test]
    #[should_panic(expected = "Guess value must be less than or equal to 100")]
    #[ignore]
    fn greater_than_100() {
        Guess::new(200);
    }

    #[test]
    fn internal() {
        assert_eq!(5, internal_adder(3,2));
    }
}

#[derive(Debug)]
pub struct Rectangle {
    length: u32,
    width: u32,
}

impl Rectangle {
    pub fn can_hold(&self, other: &Rectangle) -> bool {
        self.length > other.length && self.width > other.width
    }
}

pub fn greeting(name: &str) -> String {
    format!("Hello {}!", name)
}

pub struct Guess {
    value: u32,
}

impl Guess {
    pub fn new(value: u32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}", value);
        }

        Guess {
            value
        }
    }
}

fn internal_adder(a: i32, b: i32) -> i32 {
    a + b
}

pub mod adders{
    fn increment(a: i32)-> i32 {
//        internal_adder(a, 1) // internal_adder isn't visible in this scope!
        a+1
    }

    pub fn add_two(a: i32) -> i32 {
        a+2
    }

}

