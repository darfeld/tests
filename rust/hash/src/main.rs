use std::collections::HashMap;

fn main() {
    println!("Hello, world!");
let mut scores = HashMap::new();

    scores.insert(String::from("Blue"),10);
    scores.insert(String::from("Yellow"), 50);

    let teams = vec![String::from("Red"), String::from("Purple")];
    let initial_scores = vec![10, 50];

    let scores_bis: HashMap<_,_> = teams.iter().zip(initial_scores.iter()).collect();
    println!("scores :");
    for (key, value) in &scores{
        println!("{}: {}", key, value);
    }
    println!("scores_bis :");
    for (key, value) in &scores_bis{
        println!("{}: {}", key, value);
    }

    scores.insert(String::from("Blue"), 75 );
    println!("scores : {:?}", scores);

    scores.entry(String::from("Red")).or_insert(50);
    scores.entry(String::from("Yellow")).or_insert(100);

    println!("{:?}",scores);

    let bytes = "Truc".as_bytes();
    let mut hm = HashMap::new();
    for letter in bytes.iter(){
        let count = hm.entry(letter).or_insert(0);
        *count += 1;
    }
    println!("nombre de T dans Truc : {}", hm[&b'T']);

}
