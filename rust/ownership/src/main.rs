fn main() {
    let s = ", world!"; // pushed on the stack

    let mut sh = String::from("hello"); // allocated on the heap

    sh.push_str(s); 

    println!("{}",sh);
}
