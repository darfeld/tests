#[derive(Debug)]
enum UsState {
    Alaska,
    Alabama,
    Kentuky,
}

enum Coin{
    Penny,
    Nickel,
    Dime,
    Quarter(UsState),
}

fn value_in_cent(coin: Coin)->u32{
    match coin{
        Coin::Penny => {
            println!("lucky penny!");
            1
        },
        Coin::Nickel => 5,
        Coin::Dime => 10,
        Coin::Quarter(state) => {
            println!("State quarter from {:?}!", state);
            25
        },
    }
}

fn main() {
    println!("penny is {}",value_in_cent(Coin::Penny));
    println!("nickel is {}",value_in_cent(Coin::Nickel));
    println!("dime is {}",value_in_cent(Coin::Dime));
    println!("quarter is {}",value_in_cent(Coin::Quarter(UsState::Alaska)));
    
}
