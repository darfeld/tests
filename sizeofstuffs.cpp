#include <iostream>

using namespace std;

int main(){
	int a = 5;
	
	cout << "sizeof int : " << sizeof(int) << endl;
	cout << "sizeof char : " << sizeof(char) << endl;
	cout << "sizeof a (int) : " << sizeof(a) << endl;
	return 0;
}
