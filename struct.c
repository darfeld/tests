#include <stdio.h>

main(){
	struct { int a; int b; } C;

	C.a = 1;
	C.b = 2;

	printf("C.a : %d\nC.b : %d\n", C.a, C.b);
}
