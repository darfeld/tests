#include <stdio.h>
#include <stdlib.h>

main(){
	FILE * f=fopen("/boot/grub/grubenv","r");
	char * line = NULL;
//	ssize_t read = 0;
	size_t len=0;
	char * st=NULL;

	stat("/boot/grub/grubenv", &st);

	if ( f== NULL)
		exit(EXIT_FAILURE);
	
	while(getline(&line, &len, f) != -1 ){
		if(line[0] != '#')
			printf(line);
	}

	fclose(f);
}
