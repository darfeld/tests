#!/bin/env bash

#Parse the options
TEMP=`getopt -o :ahb: -l along,blong: -- "$@"` 

#check invalid options
if [[ $? -ne 0 ]]
then 
    echo "Invalid option"
fi

#evaluate every options
eval set -- "$TEMP"

while true
do
    case "$1" in
        -b | --blong)
            echo "-b was triggered! parameter: $2"
            shift 2
            ;;
        -a | --along)
            echo "-a was triggered!"
            shift 1
            ;;
        :)
            echo "Option: -$2 requires an argument."
            break
            ;;
        --) 
            echo "what is it? $1 $2"
            shift
            break
            ;;
        -h)
            echo " h : $1"
            shift
            break
            ;;
    esac
done

#Do stuffs... here I just check how some variable are changed 
echo "# : $#"
echo "@ : $@"
echo "1 : $1"
