#!/bin/env bash

#default values
file=""
message=""
address=""
port=""
timeout=1

function displayUsage {
    echo "usage :"
    echo "sendUDPRaw.sh -a <ip address> -p <port> -m <message> [options]"
    echo "option -t: timeout in seconds"
    echo "option -f: file with predefine messages"
}

#Parse the options
TEMP=`getopt -o :f:m:a:p:t:h -l help,file:,message:,address:,port:,timeout: -- "$@"` 

#check invalid options
if [[ $? -ne 0 ]]
then 
    echo "Invalid option"
    displayUsage
    exit 1
fi

#evaluate every options
eval set -- "$TEMP"

while true
do
    case "$1" in
        -f | --file)
            file=$2
            echo "file : $2"
            shift 2
            ;;
        -m | --message)
            message=$2
            echo "message : $2"
            shift 2
            ;;
        -a | --address)
            address=$2
            echo "address is $2"
            shift 2
            ;;
        -p | --port)
            port=$2
            echo "port is $2"
            shift 2
            ;;
        -t | --timeout)
            timeout=$2
            echo "timeout is $2"
            shift 2
            ;;
        :)
            echo "Option: -$2 requires an argument."
            displayUsage
            exit 1
            ;;
        --) 
            break
            ;;
        -h | --help)
            displayUsage
            exit 0
            ;;
    esac
done


if [[ ( ( "$message" == "" ) || ( "$address" == "" ) || ( "$port" == "" ) ) ]] 
then
    echo "Not enough information provided"
    displayUsage
#    exit 1
fi

if [[ $file != "" ]]
then
    raw_message=`grep $message $file | awk 'BEGIN{FS=":"}{print $2}'`
    echo "raw message is $raw_message"
else
    raw_message=$message
fi

echo -n $raw_message | xxd -r -p | nc -u $address $port -q$timeout
