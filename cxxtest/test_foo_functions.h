#include <cxxtest/TestSuite.h>
#include <iostream>

extern "C"{
       	int func_true();
}

class FooFunctionTestSuite : public CxxTest::TestSuite {
public:
	void test_func_true(){
		TS_ASSERT(func_true());
	}
};
