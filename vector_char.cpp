#include <iostream>
#include <string>
#include <vector>

int main(int argc, char ** argv){
	std::vector<char> vect;
	std::string str = "hello world!";
	std::copy(str.begin(),str.end(),std::back_inserter(vect));
	char * c = &vect[0];

	std::cout << c << std::endl;
	return 0;
}
