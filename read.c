#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

int main(){
	char buffer [20] = {1};


	printf("buffer size is %d\n", sizeof(buffer));

	int fd = open("CHANGELOG", O_RDONLY);

	if (fd == -1)
	{
		err(1, "%s", "CHANGELOG");
	}

	int size = read(fd, buffer, sizeof(buffer)-4);
	if( size == -1)
	{
		err(2, "buffer");
	}
	printf("%d bytes read", size);
	printf("buffer content is %s\n", buffer);
	printf("buffer last char is %d\n", buffer[sizeof(buffer)-1]);

	return 0;
}
