# testPerso
divert test persos pour moi-même

## maps
exemple of use of map<> in CPP

## segfault
the smallest segfault without warning I can write

## seg
the smallest segfault I know, with a compilation warning

## statics
tests of statics

## sizeofstuffs
give the size of several stuffs

## template
tests of CPP template

## vector_char
put a string in a char vector and get a char* from the vector

## floatToStringLimited
exemple of string formating with cout. (CPP)

## random_step
I have no idea...

## file_cp
copy a file using file streams

## memcpy_test
Playing with memcpy in cpp

## testopt.py
test getopt module for python: retrive command line options

## tests_imbriques.c
terrible switch implementation using ?: test syntax

## testGetOpt.sh
Exemple of parameters parsing with bash getopt

## threads.cpp
Exemple of threads usage in cpp

## gtest
TODO: write unitary test for some of those programs...

## parse_args.py and parse_args.sh
playing with python and bash, parsing args, whithout getopt or getopts

## sendUDPRaw.sh
a small script to send hexadecimal UDP message

## const_c*
use case for const in C/C++

## libs/
test creating and using dynamic libraries without installing them

## grubenv.c
read grub environment variables

## struct.c
make a variable of unamed struct

## foo_functions.c/.h: 
dummy functions to test googletest for C code
