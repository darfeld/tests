#include <stdio.h>

int addInt(int a, int b)
{
	return a+b;
}

int computeStuff( int (*fOperation)(int,int), int a, int b)
{
	return (*fOperation)(a,b);
}

main(){
	int (*fPtr)(int,int) = &addInt;

	printf("1 + 2 = %d\n", (*fPtr)(1,2));

	int helloWorld(){
		printf("Hello World\n");
	}

	helloWorld();

	int subInt(int a, int b){
		return a - b;
	}

	printf("2 - 1 = %d\n", subInt(2,1));

	printf("compute stuff : 3*4 = %d\n",computeStuff{int multInt( int a, int b){ return a*b;} return &multInt;},3,4);

}
