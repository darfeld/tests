#include "stdio.h"

int main(){
	const char * string = "hello";

//	this line don't compile because const protect the whole string
//	string[2]='h';
	return 0;
}
